<?xml version="1.0" encoding="UTF-8"?>
<tileset name="tileset" tilewidth="25" tileheight="25" tilecount="64" columns="4">
 <image source="tileset.png" width="100" height="400"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="unity:tag" value="HURTS"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
 <tile id="32">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="25" height="25"/>
  </objectgroup>
 </tile>
</tileset>
