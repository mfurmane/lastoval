﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationRatata : MonoBehaviour {

	enum WaveDirection { LEFT_BEFORE_ZERO, LEFT, RIGHT, RIGHT_AFTER_ZERO };
	private WaveDirection waveDirection = WaveDirection.LEFT;

	public Vector3 point;
	public float speed = 0.3f;
	public float realSpeed = 0;
	private float tileSize = 0.25f;

	// Use this for initialization
	void Start () {
		point = transform.position+new Vector3(point.x*tileSize, -point.y*tileSize);
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAround (point, new Vector3(0,0,1), realSpeed);
		//if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 90 && transform.eulerAngles.z < 270) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 90 && transform.eulerAngles.z <= 270)) {			
		if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 70 && transform.eulerAngles.z < 290) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 70 && transform.eulerAngles.z <= 290)) {			

	//	if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 100 && transform.eulerAngles.z < 250) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 100 && transform.eulerAngles.z <= 260)) {			
		//if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 70 && transform.eulerAngles.z < 290) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 70 && transform.eulerAngles.z <= 290)) {			
						speed = -speed;
						waveDirection = getOppositeDirection (waveDirection);
					}

	}
	WaveDirection getOppositeDirection(WaveDirection direction) {
				return direction == WaveDirection.RIGHT ? WaveDirection.LEFT : WaveDirection.RIGHT;
			}

	public void LetsTheGameBegin() {
		realSpeed = speed;
	}
}
