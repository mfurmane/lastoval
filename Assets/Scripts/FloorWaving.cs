﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorWaving : MonoBehaviour {

	enum WaveDirection { LEFT_BEFORE_ZERO, LEFT, RIGHT, RIGHT_AFTER_ZERO };

	private Quaternion startRotation;
	public Vector3 speed = Vector3.zero;
	private WaveDirection waveDirection = WaveDirection.LEFT;

	// Use this for initialization
	void Start () {
		startRotation = transform.rotation;
//		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.RotateAroundLocal (new Vector3 (0, 0, 0), speed.z);
//		transform.eulerAngles += speed;
//		if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 90 && transform.eulerAngles.z < 270) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 90 && transform.eulerAngles.z <= 270)) {			
//			speed.Set (0, 0, -speed.z);
//			waveDirection = getOppositeDirection (waveDirection);
//		}
	}

//	WaveDirection getOppositeDirection(WaveDirection direction) {
//		return direction == WaveDirection.RIGHT ? WaveDirection.LEFT : WaveDirection.RIGHT;
//	}

	public void LetsTheGameBegin() {
		speed = new Vector3 (0, 0, 0.25f);
	}

}

