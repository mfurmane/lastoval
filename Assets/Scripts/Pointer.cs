﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Pointer : MonoBehaviour {

	public enum Activities {PLAY, CREDITS};

	public int optionsCount = 2;
	public int activePositionId = 0;
	public Activities currentActivity;// = Activities.PLAY;
	public string firstScene;

	private Vector3[] possiblePositions;// = new Vector3[optionsCount];
	private Activities[] possibleActivities;// = new Vector3[optionsCount];

	// Use this for initialization
	void Start () {
		currentActivity = Activities.PLAY;
		possiblePositions = new Vector3[optionsCount];
		possiblePositions[0] = new Vector3 (0, 0.27f);
		possiblePositions[1] = new Vector3(0, -0.23f);
		possibleActivities = new Activities[optionsCount];
		possibleActivities [0] = Activities.PLAY;
		possibleActivities [1] = Activities.CREDITS;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.DownArrow))
			activePositionId++;
		if (Input.GetKeyDown (KeyCode.UpArrow))
			activePositionId--;

		if (activePositionId < 0)
			activePositionId = possiblePositions.Length - 1;
		if (activePositionId == possiblePositions.Length)
			activePositionId = 0;

		transform.position = possiblePositions [activePositionId];
		currentActivity = possibleActivities [activePositionId];

		if (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.Space)) {
			if (currentActivity == Activities.PLAY) {
				SceneManager.LoadScene (firstScene);
			}
		}

	}
}
