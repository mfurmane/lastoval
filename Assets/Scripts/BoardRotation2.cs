﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardRotation2 : MonoBehaviour {

	enum WaveDirection { LEFT_BEFORE_ZERO, LEFT, RIGHT, RIGHT_AFTER_ZERO };
	private WaveDirection waveDirection = WaveDirection.LEFT;

	public Vector3 point;
	public float speed = 5;
	private float tileSize = 0.25f;

	// Use this for initialization
	void Start () {
		point = transform.position+new Vector3(point.x*tileSize, -point.y*tileSize);
	}

	// Update is called once per frame
	void Update () {
		transform.RotateAround (point, new Vector3(0,0,1), speed);
		if ((waveDirection == WaveDirection.LEFT && transform.eulerAngles.z >= 178 && transform.eulerAngles.z < 181) || (waveDirection == WaveDirection.RIGHT &&  transform.eulerAngles.z > 179 && transform.eulerAngles.z <= 181)) {			
			speed = -speed;
			waveDirection = getOppositeDirection (waveDirection);
		}

	}

	WaveDirection getOppositeDirection(WaveDirection direction) {
		return direction == WaveDirection.RIGHT ? WaveDirection.LEFT : WaveDirection.RIGHT;
	}

}
